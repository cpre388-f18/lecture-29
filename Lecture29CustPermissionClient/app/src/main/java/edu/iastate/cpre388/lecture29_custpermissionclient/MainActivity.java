package edu.iastate.cpre388.lecture29_custpermissionclient;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    /** The permission string as defined in the other app. */
    private static final String MY_PERMISSION
            = "edu.iastate.cpre388.lecture29_custpermission.permission.MY_PERMISSION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v) {
        if (checkSelfPermission(MY_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
            startAct();
        } else {
            String[] perms = {MY_PERMISSION};
            requestPermissions(perms, 1);
        }
    }

    /**
     * Helper function that launches the protected Activity.  This does not test permissions.
     */
    private void startAct() {
        ComponentName protectedActivity = new ComponentName(
                "edu.iastate.cpre388.lecture29_custpermission",
                "edu.iastate.cpre388.lecture29_custpermission.ProtectedActivity");
        Intent explicitIntent = new Intent();
        explicitIntent.setComponent(protectedActivity);
        startActivity(explicitIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // This is very bad code.  Look at other sample code for good practices.
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startAct();
        }
    }
}
